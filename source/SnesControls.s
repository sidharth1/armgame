.section	.text
/*
*Resets the values of the game so that the initial game is launched 
*/
.globl	InitializeGame
InitializeGame:
	push	{r4-r7, lr}
	ldr	r4,	=NewBoardStruct		//Load the initial board structure
	ldr	r6,	=boardStruct		//Load the board structure that will be used throughtout the game.
	mov	r7,	#0					//Initialize counter
copyLoop:
	cmp	r7,	#256				//Check if counter has been reached
	bge	endCopy					//If it has, go to endCopy
	ldrb	r5,	[r4]			//Load offset from the intial board
	strb	r5,	[r6]			//And store it in the board structure
	add	r4,	#1					//Increment value of initial board
	add	r6,	#1					//Increment value of board structure
	add	r7,	#1					//Increment counter
	b	copyLoop
endCopy:
	ldr	r4,	=PlayerOffset		//Load the address of player offset
	mov	r5,	#225
	str	r5,	[r4]				//Store the starting position (225) of the player into player offset
	ldr	r4,	=KeysPicked			//Load the address of KeysPicked
	mov	r5,	#0
	str	r5,	[r4]				//Store the starting value of keysPicked by the player (0) into KeysPicked
	ldr	r4,	=MovesLeft			//Load the address of movesLeft
	mov	r5,	#150
	str	r5,	[r4]				//Store the initial value of the number of moves the player has left (150) in MovesLeft
	ldr	r4,	=length				//Load the address of length
	mov	r5,	#3
	str	r5,	[r4]				//Store the initial value of length in length
	ldr	r4,	=totalKeys			//Load the address of totalKeys
	mov	r5,	#3
	str	r5,	[r4]				//Store the initial value of the number of keys on the board (3) in totalKeys
	ldr	r4,	=WinFlag			//Load the WinFlag
	mov	r5,	#0
	strb	r5,	[r4]			//Store the initial value of the Win Flag (0) in WinFlag
	ldr	r4,	=LoseFlag			//Load the address of LoseFlag
	mov	r5,	#0
	strb	r5,	[r4]			//Store the initial value of the Lose Flag(0) in LoseFlag
	ldr	r4,	=PlayerInfo			//Load the address of PlayerInfo
	mov	r5,	#40
	str	r5,	[r4]				//Store the initial value of the x coordinate of the player location in PlayerInfo
	mov	r5,	#560
	str	r5,	[r4, #4]			//Store the initial value of the y coordinate of the player location in PlayerInfo+4
	ldr	r0,	=PlayerOffset		//Load the player offset into r0
	ldr	r0,	[r0]
	bl	InitGameScreen			//Display the game screen
endInitializeGame:	
	pop	{r4-r7, pc}				//Continue with program

/*
*Launches the Game Won Message
*/
.globl	InitGameWon
InitGameWon:
	push	{r4-r7, lr}
	ldr	r4,	=GameWonSprite	//Load the screen that displays the message "Game Won"
	ldr	r6,	=boardStruct	//Load the board structure that will be used throughtout the game.
	mov	r7,	#0				//Initialize counter
copyLoop1:
	cmp	r7,	#256			//Check if counter has been reached
	bge	endCopy1			//If it has, go to endCopy1
	ldrb	r5,	[r4]		//Load the offset from the Game Won Screen
	strb	r5,	[r6]		//And store it in the board structure
	add	r4,	#1				//Increment value of Game Won Screen
	add	r6,	#1				//Increment value of board structure
	add	r7,	#1				//Increment counter
	b	copyLoop1
endCopy1:
	ldr	r4,	=totalKeys		//Load address of totalKeys
	mov	r5,	#0
	str	r5,	[r4]			//Set number of keys on the board to 0
	bl	InitGameScreen		//Display the game screen
endInitGameWon:	
	pop	{r4-r7, pc}

/*
*Launches the Game Lost Message
*/
.globl	InitGameLost
InitGameLost:
	push	{r4-r7, lr}
	ldr	r4,	=GameLostSprite	//Load the screen that displays the message "Game Lost"
	ldr	r6,	=boardStruct	//Load the board structure that will be used throughout the game
	mov	r7,	#0				//Initialize counter
copyLoop2:
	cmp	r7,	#256			//Check if counter has been reached
	bge	endCopy2			//If it has, go to endCopy2
	ldrb	r5,	[r4]		//Load the offset from the Game Lost Screen
	strb	r5,	[r6]		//And store it in the board structure
	add	r4,	#1				//Increment value of game lost screen
	add	r6,	#1				//Increment value of board structure
	add	r7,	#1				//Increment counter
	b	copyLoop2
endCopy2:
	ldr	r4,	=totalKeys		//Load address of totalKeys
	mov	r5,	#0
	str	r5,	[r4]			//Set number of keys on the board to 0
	bl	InitGameScreen		//Display the game screen
endInitGameLost:	
	pop	{r4-r7, pc}


.section	.data
.globl	NewBoardStruct
NewBoardStruct:	//1-wall tile, 0-floor tile, 2-key, 3-door, 4-Exit Door, 5-Person
	.byte	1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1
	.byte	1, 0, 0, 0, 0, 0, 0, 0, 3, 0, 0, 0, 0, 0, 0, 1
	.byte	1, 0, 1, 1, 1, 0, 1, 0, 1, 1, 1, 1, 1, 1, 0, 1
	.byte	1, 0, 1, 2, 1, 0, 1, 0, 1, 0, 0, 0, 0, 1, 0, 1

	.byte	1, 0, 1, 0, 1, 0, 1, 0, 3, 0, 1, 0, 0, 1, 0, 1
	.byte	1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 0, 1, 0, 1
	.byte	1, 0, 0, 0, 0, 0, 1, 0, 1, 0, 1, 0, 0, 1, 0, 1
	.byte	1, 1, 1, 1, 1, 1, 1, 3, 1, 0, 1, 0, 1, 1, 0, 1

	.byte	1, 2, 0, 0, 0, 0, 1, 0, 1, 0, 1, 0, 3, 0, 0, 1
	.byte	1, 1, 1, 1, 1, 0, 1, 0, 1, 0, 1, 0, 1, 1, 1, 1
	.byte	1, 0, 0, 0, 0, 0, 1, 0, 1, 0, 1, 0, 0, 0, 0, 1
	.byte	1, 0, 1, 1, 1, 1, 1, 0, 1, 0, 1, 1, 1, 1, 0, 1

	.byte	1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 1
	.byte	1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 0, 1, 0, 1
	.byte	1, 5, 0, 0, 0, 0, 0, 0, 1, 2, 0, 0, 0, 1, 0, 1
	.byte	1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 4, 1
