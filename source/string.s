.section	.text

/* 
*Draws a string which is passed in
*/
.globl	DrawString
DrawString:
	push	{r4-r10, lr}
	mov	r4,	r0	//address of string to print
	mov	r6,	r1	//Starting x coordinate
	mov	r7,	r2	//Starting y coordinate
	ldrb	r5,	[r4]	//Load value of string into r5
StringLoop:
	cmp	r5,	#0	//Check if string is 0
	beq	endDrawString$	//If it is, endDrawString$
	mov	r0,	r6	//set x	
	mov	r1,	r7	//set y
	mov	r2,	r5	//char to print	
	bl	DrawChar	//print char	
	add	r6,	#10	//update x = x + 10
	add	r4,	#1	//update address to next char in string
	ldrb	r5,	[r4]	//next char to print in r5
	cmp	r5,	#'X'	//Check if character is 'X'
	beq	printMoves	//If it is, then go to printMoves
	cmp	r5,	#'Z'	//If it is 'Z' then go to print Keys.
	beq	KeyPrint
	b	StringLoop

printMoves:
	ldr	r8,	=MovesLeft	//Load the value of Moves left into r8
	ldr	r8,	[r8]
	mov	r9,	#3	
	cmp	r8,	#100	//Check if moves left is greater 100
	movlt	r9,	#2	//If it is greater than 100, set r9 to 2
	cmp	r8,	#10		//Check if moves left is greater 10
	movlt	r9,	#1	//If it is greater than 10, set r9 to 1
	ldr	r8,	=length	//Load address of length
	str	r9,	[r8]	//Set length to value of r9
	
	bl	convertToAscii
	bl	clearMoves
	ldr	r8,	=length
	ldrb	r8,	[r8]
	ldr	r9,	=arrayAscii
printLoop:	//to print moves
	cmp	r8,	#0		//Check if  length is 0
	ble	endDrawString$
	ldrb	r10,	[r9]		//r10 containts char to print
	//compare to ascii chart and print
	mov	r0,	r6	//place x
	mov	r1,	r7	//place y	
	cmp	r10,	#0	//check if char to print is 0
	moveq	r2,	#'0'
	cmp	r10,	#1
	moveq	r2,	#'1'	//check if char to print is 1
	cmp	r10,	#2
	moveq	r2,	#'2'	//check if char to print is 2
	cmp	r10,	#3
	moveq	r2,	#'3'	//check if char to print is 3
	cmp	r10,	#4
	moveq	r2,	#'4'	//check if char to print is 4
	cmp	r10,	#5
	moveq	r2,	#'5'	//check if char to print is 5
	cmp	r10,	#6
	moveq	r2,	#'6'	//check if char to print is 6
	cmp	r10,	#7
	moveq	r2,	#'7'	//check if char to print is 7
	cmp	r10,	#8
	moveq	r2,	#'8'	//check if char to print is 8
	cmp	r10,	#9
	moveq	r2,	#'9'	//check if char to print is 9
	cmp	r10,	#'X'	//check if char to print is 'X' which is a placeholder
	moveq	r2,	#'0'	
	addeq	r8,	#1	//increment length by 1
	beq	updateLoop	//branch back
	bl	DrawChar
updateLoop:
	add	r6,	#10	//add 10 to x
	sub	r8,	#1	//add 1 to length
	add	r9,	#1	//add 1 to arrayAscii
	b	printLoop	//branch back
//
KeyPrint:
	bl	clearKeys		//clear the keys area to print new value
	ldr	r8,	=KeysPicked	//load number of keys left
	ldr	r8,	[r8]
	cmp	r8,	#0
	moveq	r5,	#'0'		//0 if no keys
	cmp	r8,	#1
	moveq	r5,	#'1'		//1 if we have keys left
	b	StringLoop

endDrawString$:
	pop	{r4-r10, pc}


.globl	clearMoves
clearMoves:
	push	{r4-r6, lr}
	mov	r4,	#400	//x pos to clear moves area
	mov	r5,	#660	//y pos to clear moves area
	mov	r6,	#35	//dimension of area to clear
clearLoop:
	cmp	r6,	#0	//decrement is a loop and draw squares within squares
	ble	endClearLoop
	mov	r0,	r4
	mov	r1,	r5
	mov	r2,	#0	//black colour to clear
	mov	r3,	r6	//r3 is dimension of square to draw
	bl	DrawSquare	//draw the square
	add	r4,	#1	//increment x
	add	r5,	#1	//increment y
	sub	r6,	#1	//decrement dimension by 1
	b	clearLoop	//branch back
endClearLoop:
	pop	{r4-r6, pc}

.globl	clearKeys
clearKeys:
	push	{r4-r6, lr}
	ldr	r4,	=380	//x pos to clear keys area
	ldr	r5,	=700	//y pos to clear keys area
	mov	r6,	#30	//dimension of square to draw
clearLoop1:
	cmp	r6,	#0	//dimension if decremented by 1 to fill square within square and clear area
	ble	endClearLoop1	
	mov	r0,	r4	//mov x pos
	mov	r1,	r5	//move y pos
	mov	r2,	#0	//black colour to fill
	mov	r3,	r6	//dimension of square
	bl	DrawSquare
	add	r4,	#1	//add 1 to x
	add	r5,	#1	//add 1 to y
	sub	r6,	#1	//decrement dimension by 1
	b	clearLoop1	//branch back
endClearLoop1:
	pop	{r4-r6, pc}


.globl	convertToAscii
convertToAscii:
	push	{r4-r7, lr}
	ldr	r4,	=MovesLeft	//load number of moves left
	ldr	r4,	[r4]		
	mov	r5,	r4		//moves left into r5
	mov	r6,	#0		//r6 is counter
	ldr	r7,	=arrayAscii	//load asciiArray which stores each numerical value to be printed
	add	r7,	#2		//add 2 to r7
divloop:
	cmp	r5,	#10		//subtract till less than 10 (i.e. divide by 10)
	blt	resetDiv		//branch to resetDiv if less than 10
	sub	r5,	#10
	add	r6,	#1
	b	divloop
resetDiv:
	strb	r5,	[r7]		//store the number into the third position in arrayAscii
	sub	r7,	#1		//subtract 1 from arrayAscii to put the next number in
	cmp	r6,	#10		//check if counter is greater than 10
	movge	r5,	r6		//if so, move counter value into r5
	movge	r6,	#0		//reset counter to 0
	bge	divloop			//branch to divLoop
	strb	r6,	[r7]		//else store the number into r7
	bl	replaceChar		//branch to replaceChar
	pop	{r4-r7, pc}

.globl	replaceChar
replaceChar:
	push	{r4-r6, lr}
	ldr	r4,	=length		//load the length of the number of moves left
	ldrb	r4,	[r4]
	ldr	r5,	=arrayAscii	//load the arrayAscii
	mov	r6,	#'X'		//move X as a placeholder into r6
	cmp	r4,	#3		//compare length to 3 digits
	beq	doneReplace		//if 3, no replacement needed with a placeholder
	cmp	r4,	#2		//otherwise check if length of moves left is 2
	beq	store1			//put 1 X as a placeholder for the first pos in arrayAscii
	cmp	r4,	#1		//check if length is 1
	beq	store2			//put 2 placeholders X, X into pos 1 and 2 of arrayAscii
	b	doneReplace		//branch to done otherwise
store1:
	strb	r6,	[r5]		//put 1 placeholder into arrayAscii
	b	doneReplace
store2:
	strb	r6,	[r5]		//put 2 placeholders into arrayAscii
	strb	r6,	[r5, #1]	//
doneReplace:
	pop	{r4-r6, pc}

.section	.data

//*****Strings
.globl	Title, Moves, length, arrayAscii
Title:	.asciz	"Bored Amaze"
Moves:	.asciz	"Actions Left: X"
length:	.byte	3	//3
arrayAscii:	.byte	'X','X','X'

