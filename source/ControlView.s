.section	.text
//method to move player

/*
*Args: r0: 1-right, 2-left, 3-up, 4-down, 5-open door
*CheckPlayerPos receives an input of what the player is trying to do. It checks if the move is valid then implements the move if it is. It also checks if the players
*destination target is a wall, door, tile or exit door.
*/
.globl	ChangePlayerPos
ChangePlayerPos:
	push	{r4-r10, lr}
	mov	r4,	r0	//move requested
	ldr	r5,	=PlayerOffset
	ldr	r5,	[r5]	//current offset of player 
	ldr	r7,	=boardStruct	//Load the boardStructure into r7
rightCheck:
	cmp	r4,	#1	//Check if the player wants to move right
	bne	leftCheck	//If not, then move to the next direction check
	add	r6, r5, #1	//Add 1 to the player offset to check tile to the right
	bl	wallcheck	//Check if it is a wall	
	ldr	r0,	=MovesLeft	//Get the moves player has left
	ldr	r1,	[r0]		
	sub	r1,	#1			//Decrement it by 1 and store it again
	str	r1,	[r0]
	mov	r0,	r6		//Load the new player offset into r0
	bl	CheckKeys	//Check if it is a key space
	mov	r8,	#0		
	strb	r8,	[r7, r5]!	//Store 0 in the old player offset in board structure and update r7
	mov	r8,	#5		
	strb	r8,	[r7, #1]!	//Store 5 in the new player offset and update r7
	mov	r0,	r6
	bl	InitGameScreen		//Initialize the game screen
	add	r5,	#1
	ldr	r6,	=PlayerOffset		
	str	r5,	[r6]			//Store the new value of player offset
	b	endChangePlayerPos$
leftCheck:
	cmp	r4,	#2	//Check if the player wants to move right
	bne	upCheck	//If not, then move to the next direction check
	sub	r6, r5, #1	//Subtract 1 to the player offset to check tile to the left
	bl	wallcheck	//Check if it is a wall		
	ldr	r0,	=MovesLeft	//Get the moves player has left
	ldr	r1,	[r0]
	sub	r1,	#1		//Decrement it by 1 and store it again
	str	r1,	[r0]
	mov	r0,	r6		//Load the new player offset into r0
	bl	CheckKeys	//Check if it is a key picked
	ldrb	r10, [r7, r6]	//Load new player offset value into r10
	cmp	r10, #2		//Check if it is a key
	ldreq	r9, =KeysPicked	//If it is, then increment the number of keys collected
	ldreq	r10, [r9]
	addeq	r10, #1
	streq	r10, [r9]		//Store the new value of KeysPicked
	mov	r8,	#0
	strb	r8,	[r7, r5]!	//Store 0 in the old player offset in board structure and update r7
	mov	r8,	#5
	sub	r7,	#1
	strb	r8,	[r7]		//Store 5 in the new player offset and update r7
	mov	r0,	r6
	bl	InitGameScreen	//Initialize the game screen
	sub	r5,	#1
	ldr	r6,	=PlayerOffset
	str	r5,	[r6]		//Store the new value of player offset
	b	endChangePlayerPos$
upCheck:
	cmp	r4,	#3	//Check if the player wants to move up
	bne	downCheck	//If not, then move to the next direction check
	sub	r6, r5, #16	//Subtract 16 to the player offset to check tile upwards
	bl	wallcheck	//Check if it is a wall
	ldr	r0,	=MovesLeft	//Get the moves player has left
	ldr	r1,	[r0]
	sub	r1,	#1		//Decrement it by 1 and store it again
	str	r1,	[r0]
	mov	r0,	r6		//Load the new player offset into r0
	bl	CheckKeys	//Check if it is a key picked
	ldrb	r10, [r7, r6]	//Load new player offset value into r10
	cmp	r10, #2		//Check if it is a key
	ldreq	r9, =KeysPicked	//If it is, then increment the number of keys collected
	ldreq	r10, [r9]
	addeq	r10, #1
	streq	r10, [r9]		//Store the new value of KeysPicked
	mov	r8,	#0
	strb	r8,	[r7, r5]!	//Store 0 in the old player offset in board structure and update r7
	mov	r8,	#5
	sub	r7,	#16
	strb	r8,	[r7]		//Store 5 in the new player offset and update r7
	mov	r0,	r6
	bl	InitGameScreen		//Initialize the game screen
	sub	r5,	#16
	ldr	r6,	=PlayerOffset
	str	r5,	[r6]		//Store the new value of player offset
	b	endChangePlayerPos$
downCheck:	
	cmp	r4,	#4		//Check if the player wants to move right
	bne	openDoor	//If not, then move to see if the player is trying to open a door
	add	r6, r5, #16	//add 16 to the player offset to check tile below
	bl	wallcheck	//Check if it is a wall
	ldr	r0,	=MovesLeft	//Get the moves player has left
	ldr	r1,	[r0]
	sub	r1,	#1		//Decrement it by 1 and store it again
	str	r1,	[r0]
	mov	r0,	r6		//Load the new player offset into r0
	bl	CheckKeys	//Check if it is a key picked
	mov	r8,	#0
	strb	r8,	[r7, r5]!	//Store 0 in the old player offset in board structure and update r7
	mov	r8,	#5
	add	r7,	#16
	strb	r8,	[r7]		//Store 5 in the new player offset and update r7
	mov	r0,	r6
	bl	InitGameScreen		//Initialize the game screen
	add	r5,	#16
	ldr	r6,	=PlayerOffset	//Store the new value of player offset
	str	r5,	[r6]
	b	endChangePlayerPos$
//-----------------
openDoor:
	cmp	r4,	#5			//Check if player is trying to open a door
	bne	endChangePlayerPos$		//If not, then return continue with program
	ldr	r8,	=KeysPicked			//Load value of keysPicked
	ldr	r9,	[r8]
	cmp	r9,	#0				//Check if the player even has keys
	beq	endChangePlayerPos$	//no keys then continue with program
	//
	add	r6,	r5,	#1			//Add 1 to current player offset to get offset of tile to the right
	ldrb	r8,	[r7,	r6]	//Load the value of that offset
	cmp	r8,	#3				//check if it is a door
	bne	checkLeft			//If it isnt, check another direction
	mov	r8,	#0				//change door to tile
	strb	r8,	[r7, r6]	//Store this in board structure
	b	initScr				//Call game screen
checkLeft:
	sub	r6,	r5,	#1			//Subtract 1 to current player offset to get offset of tile to the left
	ldrb	r8,	[r7,	r6]	//Load the value of that offset
	cmp	r8,	#3				//check if it is a door
	bne	checkUp				//If it isnt, check another direction
	mov	r8,	#0				//change door to tile
	strb	r8,	[r7, r6]	//Store this in board structure
	b	initScr				//Call game screen
checkUp:
	sub	r6, r5, #16			//Subtract 16 to current player offset to get offset of tile above
	ldrb	r8,	[r7,	r6]	//Load the value of that offset
	cmp	r8,	#3				//check if it is a door
	bne	checkDown			//If it isnt, check another direction
	mov	r8,	#0				//change door to tile
	strb	r8,	[r7, r6]	//Store this in board structure
	b	initScr				//Call game screen
checkDown:
	add	r6, r5, #16			//Add 16 to current player offset to get offset of tile below
	ldrb	r8,	[r7,	r6]	//Load the value of that offset
	cmp	r8,	#3				//check if it is a door
	bne	checkExit			//If it isnt, check if player is trying to open the exit door
	beq	endCheckDown		//If it is a door, go to endCheckDown
checkExit:
	cmp	r8,	#4				//Check if offset is an exit door
	bne	endChangePlayerPos$	//If it is not, then continue with program
	ldr	r9,	=WinFlag
	mov	r10,	#1			//If it is the exit door, then set the win flag to 1
	strb	r10,	[r9]	//Store win Flag again.
endCheckDown:
	mov	r8,	#0				//change door to tile
	strb	r8,	[r7, r6]	//Store this in board structure
	b	initScr				//Call game screen
initScr:
	ldr	r8,	=KeysPicked		//Load number of keys picked
	mov	r9,	#0				//Change it to 0 and store it again.
	str	r9,	[r8]

	mov	r0,	r5				//Move current player offset back to r0 and call the game screen.
	bl	InitGameScreen

	b	endChangePlayerPos$	//Continue with program
wallcheck:
	ldr	r9,	=boardStruct		//Load board structure
	ldrb	r10,	[r9, r6]	//Load value of new offset into r10
	cmp	r10,	#3				//check if it is a door
	beq	endChangePlayerPos$		//
	cmp	r10,	#4				//check if it is an exit door
	beq	endChangePlayerPos$
	cmp	r10,	#1				//check if it is a wall
	movne	pc,	lr
endChangePlayerPos$:
	pop	{r4-r10, pc}

//CheckKeys checks to see how many keys the player has collected, and if they are at the location of the next key. If they are, then it changes the number of keys left on the board.
.globl	CheckKeys
CheckKeys:
	push	{r4-r6, lr}
	mov	r6,	r0					//r6 is new player offset
	ldr	r4,	=totalKeys			//Load value of totalKeys into r5
	ldr	r5,	[r4]
	cmp	r5,	#3					//Check if there are 3 keys left on the board
	beq	case1					//If so, go to case1
	cmp	r5,	#2					//Check if there are 2 keys left on the board
	beq	case2					//If so, go to case2
	cmp	r5,	#1					//Check if there is 1 key left on the board
	beq	case3					//If so, go to case3
case1:
	cmp	r6,	#129				//Check if new player offset is at location of key
	moveq	r1,	#2				//If so, set totalKeys to 2
	streq	r1,	[r4]
	b	endCheckKeys
case2:
	cmp	r6,	#51					//Check if new player offset is at location of key
	moveq	r1,	#1				//If so, set totalKeys to 1
	streq	r1,	[r4]			
	b	endCheckKeys
case3:
	cmp	r6,	#233				//Check if new player offset is at location of key
	moveq	r1,	#0				//If so, set totalKeys to 0
	streq	r1,	[r4]
	b	endCheckKeys
endCheckKeys:
	pop	{r4-r6, pc}


.section	.data
.globl	WinFlag, LoseFlag
WinFlag:	.byte	0	//1-win
LoseFlag:	.byte	0	//1-lost



