.section	.data


//*****Updates (getters and setters)
.globl	PlayerOffset, MovesLeft, KeysPicked, totalKeys
PlayerOffset:	.int	225	//initial offset
MovesLeft:	.int	150	//150 moves to begin with
KeysPicked:	.int	0	//number keys picked up initially
totalKeys:	.int	3	//number of keys left on board

//*****Colour codes
.globl	maroon, white, yellow, red, green, blue, olive
maroon:	.int 	0x7800
white:	.int	0xFFFF
blue:	.int	0xF1
yellow:	.int	0xFFE0
red:	.int	0xF800
green:	.int	0x07E0
olive:	.int	0x7BE0

//*****Data Structure
.globl	PlayerInfo	//return the position of the player relative to the structure (row, column)
PlayerInfo:	.int	40, 560
.globl	boardStruct
boardStruct:	//1-wall tile, 0-floor tile, 2-key, 3-door, 4-Exit Door, 5-Person
	.byte	1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1
	.byte	1, 0, 0, 0, 0, 0, 0, 0, 3, 0, 0, 0, 0, 0, 0, 1
	.byte	1, 0, 1, 1, 1, 0, 1, 0, 1, 1, 1, 1, 1, 1, 0, 1
	.byte	1, 0, 1, 2, 1, 0, 1, 0, 1, 0, 0, 0, 0, 1, 0, 1

	.byte	1, 0, 1, 0, 1, 0, 1, 0, 3, 0, 1, 0, 0, 1, 0, 1
	.byte	1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 0, 1, 0, 1
	.byte	1, 0, 0, 0, 0, 0, 1, 0, 1, 0, 1, 0, 0, 1, 0, 1
	.byte	1, 1, 1, 1, 1, 1, 1, 3, 1, 0, 1, 0, 1, 1, 0, 1

	.byte	1, 2, 0, 0, 0, 0, 1, 0, 1, 0, 1, 0, 3, 0, 0, 1
	.byte	1, 1, 1, 1, 1, 0, 1, 0, 1, 0, 1, 0, 1, 1, 1, 1
	.byte	1, 0, 0, 0, 0, 0, 1, 0, 1, 0, 1, 0, 0, 0, 0, 1
	.byte	1, 0, 1, 1, 1, 1, 1, 0, 1, 0, 1, 1, 1, 1, 0, 1

	.byte	1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 1
	.byte	1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 0, 1, 0, 1
	.byte	1, 5, 0, 0, 0, 0, 0, 0, 1, 2, 0, 0, 0, 1, 0, 1
	.byte	1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 4, 1


