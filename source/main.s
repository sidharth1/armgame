/*
*Sidharth Jha & Shamil Ukani
*10032687 & 10100892
*This is where the program starts. It begins by calling the initial menu, then reads selection from the controller.
*
*/
.section    	.init
.globl    	 _start

_start:
    b      	 main
    
.section	.text

main:
	mov     sp, #0x8000
	
	bl	EnableJTAG
	bl	InitFrameBuffer
	bl	initSNES
StartMenu:
	bl	InitMenu		//Draws the initial menu
	bl	DrawPointerStart	//Draws the pointer to option "Start"
	ldr	r5,	=65535
	ldr	r4,	=SNESDat
	str	r5,	[r4]		
readMenuOption:
	bl	readSNES	//Read for a selection from the controller
	cmp	r5,	r0		//
	beq	readMenuOption	//If it is, then loop back to readMenuOption
	str	r0,	[r4]		//Store the choice in r4
	tst	r0,	#0b10000	//Check if player chose to move up
	beq	DrawStart		//If so, then go to DrawStart
	tst	r0,	#0b100000	//Check if player chose to move down
	beq	DrawQuit		//If so, then go to DrawQuit
	tst	r0,	#0b100000000	//Check if player chose the a button
	beq	OptionSelected	//If so, then go to OptionSelected
read:	
	ldr	r5,	[r4]	
	b	readMenuOption	//Loop back to readMenuOption
DrawStart:
	bl	DrawPointerStart	//The player is on the choice "Start", so draw a pointer beside "Start"
	b	read		
DrawQuit:
	bl	DrawPointerQuit		//The player is on the choice "Quit", so draw a pointer beside "Quit"
	b	read
OptionSelected:
	ldr	r4,	=StartFlag		//Load address of StartFlag
	ldr	r4,	[r4]
	cmp	r4,	#1				//Check if StartFlag is 1. If it is, then the player is on the "Start" option.
	bne	QuitGame			//If not, then go to quit game
	bl	ClearScreen			//Clear the screen
	bl	InitializeGame		//Create the game screen
	b	PlayBegin			//BEGIN! 
ReDraw:
	ldr	r0,	=PlayerOffset	//Load player offset into r0
	ldr	r0,	[r0]
	bl	InitGameScreen		//Initialize the draw screen
PlayBegin:
	ldr	r6,	=MovesLeft		//Load the moves left
	ldr	r5,	=65535
	ldr	r4,	=SNESDat
	str	r5,	[r4]
	ldr	r8,	=WinFlag		//Load the winFlag into r8
readInput:
	bl	readSNES	//Check if the player has pressed any buttons
	cmp	r5,	r0
	beq	readInput
//check if 150 moves over and Quit Lost
	ldr	r7,	[r6]	//Load the moves left
	cmp	r7,	#0		//Check if the player has any moves left
	beq	QuitLost	//If there are no moves left, go to Lose Game Screen
	str	r0,	[r4]
	tst	r0,	#0b1000		//Check if player has chosen start button
	beq	PauseSelect		//If so, go to pause menu
	tst	r0,	#0b100000000	//Check if player has hit 'a' button
	moveq	r4,	#5		//If so, #5 goes in r4
	tst	r0,	#0b10000	//Check if player has chosen up button
	moveq	r4,	#3		//If so, #3 goes in r4
	tst	r0,	#0b100000	//Check if player has chosen down button
	moveq	r4,	#4		//If so, #4 goes in r4
	tst	r0,	#0b1000000	//Check if player has chosen left button
	moveq	r4,	#2		//If so, #2 goes in r4
	tst	r0,	#0b10000000	//Check if player has chosen right button
	moveq	r4,	#1		//If so, #1 goes in r4
	mov	r0,	r4	
	bl	ChangePlayerPos
	ldrb	r9,	[r8]	//Load the win Flag
	cmp	r9,	#1			//Check if winFlag si set
	beq	QuitWon			//If it is, then go to win game screen
	mov	r4,	#0	
	ldr	r5,	[r4]
	b	readInput		//Go back to read next player selection

PauseSelect:
	bl	PauseMenu		//Draw Pause menu screen
	bl	DrawPointerStart	//Draw pointer to option "Restart"
	ldr	r5,	=65535
	ldr	r4,	=SNESDat
	str	r5,	[r4]
readPauseOption:
	bl	readSNES	//Read for a selection from the controller
	cmp	r5,	r0
	beq	readPauseOption
	str	r0,	[r4]	//Store player choice in r4
	tst	r0,	#0b1000	//Check if player chose start button
	beq	resume	//If so, then resume game
	tst	r0,	#0b10000	//Check if player chose to move up
	beq	DrawStart1		//If so, then go to DrawStart1
	tst	r0,	#0b100000	//Check if player chose to move down
	beq	DrawQuit1		//If so, then go to DrawQuit1
	tst	r0,	#0b100000000	//Check if player chose the a button
	beq	OptionSelected1	//If so, then to to OptionSelected
read1:
	ldr	r5,	[r4]
	b	readPauseOption	//Loop back to readPauseOption
DrawStart1:
	bl	DrawPointerStart	//The player is on the choice "Start" so draw a pointer beside "Start"
	b	read1
DrawQuit1:
	bl	DrawPointerQuit		//The player is on the choice "Quit" so draw a pointer beside "Quit"
	b	read1
OptionSelected1:
	ldr	r4,	=StartFlag		//Load address of start flag
	ldr	r4,	[r4]
	cmp	r4,	#1		//Check if startFlag is set. If it is, then pointer is on "restart" , so go back to main menu to restart the game
	bne	StartMenu	//If it is not, then go to 
	bl	ClearScreen	//Clear the screen
	bl	InitializeGame	//Re-initialize the game to its original settings
	b	PlayBegin	//BEGIN!
resume:
	bl	ClearScreen	//Clear the screen
	b	ReDraw	//ReDraw the game board
QuitLost:
	bl	InitGameLost	//Draw game lost screen
	ldr	r5,	=65535
	ldr	r4,	=SNESDat
	str	r5,	[r4]
	b	readKeyPressed	//check if the player presses a key
QuitWon:
	bl	InitGameWon		//Draw game won screen
	ldr	r5,	=65535
	ldr	r4,	=SNESDat
	str	r5,	[r4]
readKeyPressed:
	bl	readSNES	//Check if the player presses a button
	cmp	r5,	r0
	beq	readKeyPressed	//If not, then loop back to check if a button is pressed
	b	StartMenu
QuitGame:
	bl	ClearScreen	//Clear the screen


haltLoop$:
	b		haltLoop$

.section	.data
.globl	SNESDat
SNESDat:	.int	65535

