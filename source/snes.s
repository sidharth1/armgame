
/*
*Latch GPIO Line =	9	// Latch GPIO line
*Data GPIO Line = 10	// Data GPIO line
*Clock GPIO Line = 11
*Half Clock Cycle = 6
*Full Clock Cycle = 12
*/
.section .text

/*
* initializes the GPIO pins for the SNES controller
*/
.globl initSNES
initSNES:
	push	{lr}
	mov	r0, #9		//Set Latch GPIO Line
	mov	r1, #1			// GPIO code for output
	bl	setGPIOFunction
	mov	r0, #10		//Set Data GPIO Line
	mov	r1, #0			// GPIO code for input
	bl	setGPIOFunction
	mov	r0, #11		//Set Clock GPIO Line
	mov	r1, #1			// GPIO code for output
	bl setGPIOFunction
	mov	r0, #9		//Set Latch GPIO Line
	mov	r1, #0
	bl	setGPIO
	mov	r0, #11		//Set Clock GPIO Line
	mov	r1, #0
	bl 	setGPIO
	pop	{pc}

/*
* readSNES returns controller button bits in r0 so that they can be evaluated
*/
.globl readSNES
readSNES:
	push	{r1-r3, lr}
	mov 	r2,	#0		// r2 contains the bits read from the controller
	mov	r3, 	#0	// r3 contains the number of buttons read
	mov	r0, 	#9	// Set Latch line to be initially high
	mov	r1, 	#1
	bl	setGPIO
	mov	r0, 	#11	// Set clock line to be initially high
	mov	r1, 	#1
	bl 	setGPIO
	mov	r0, 	#12 // wait a full clock cycle (12) with latch set high.
	bl	loopWait	//Call loopWait
	mov	r0, 	#9	// Set latch signal to low
	mov	r1, 	#0
	bl	setGPIO

Loop2$:
	mov	r0, 	#6	//Wait a half clock cycle (6) with latch set low
	bl	loopWait
	mov	r0, 	#11	//Set clock signal to low
	mov	r1, 	#0
	bl	setGPIO
	mov	r0, 	#6	//Wait a half clock cycle (6) with clock set low
	bl	loopWait
	mov	r0, 	#10	//Reads input from the data line. If a button is pressed, it will return 0. It will return 1 if it is not pressed.
	bl	getGPIO			// returns single-bit value in r0
button:	
	lsl	r0,	 r3		// shift the returned value into position
	orr	r2, 	r0		// place the returned bit into r2
	mov	r0, 	#11	//Set clock signal to high
	mov	r1, 	#1	
	bl	setGPIO
	add	r3, 	#1		// increment the number of bits read
	cmp	r3, 	#16		// test if bits 0-15 have been read
	beq	doneLoop		// branch if r3 is less than 16
	b	Loop2$
doneLoop:
	mov	r0, r2		// return value
	ldr	r1, =0xffff
	and 	r0, r1
	pop	{r1-r3, pc}
/*
*getGPIOAddress simply gets the address of the GPIO
*/

.global getGPIOAddress
getGPIOAddress:
	push	{lr}
	ldr	r0,	=0x20200000
	pop	{pc}

/*
*setGPIOFunction sets the GPIO function, and takes the pin number in r0
*And the value to set the GPIO in r1
*/
.global setGPIOFunction
setGPIOFunction:
	push	{r2-r5, lr}

	baseAddr	.req	r0	//base address for GPIO
	selectMask	.req	r1	//value to write to the input register
	bit		.req	r2	//the offset for the pin set in memory
	pinSet		.req	r3	//the offset for the pin in the pin set
	data		.req	r4	//data loaded from memory
	
	mov	bit,	r0
	mov	pinSet,	#0
	
	bl	getGPIOAddress
	
calcAddr:
	cmp	bit,	#9
	subgt	bit,	#10
	addgt	pinSet,	#4
	bgt	calcAddr
//r5 is used as a r4orary storage while the data is changed
	ldr	data,	[baseAddr, pinSet]
	mov	r5,	#7	//Binary number 111
	lsl	r5,	bit	//Left shift by the bit three times 
	lsl	r5,	bit
	lsl	r5,	bit	//
	bic	data,	r5	//Clear the data bits in r5
	mov	r5,	selectMask
	lsl	r5,	bit
	lsl	r5,	bit
	lsl	r5,	bit	//Left shift by the bit three times
	orr	data,	r5
	str	data,	[baseAddr, pinSet]
	.unreq	baseAddr
	.unreq	selectMask
	.unreq	bit
	.unreq	pinSet
	.unreq	data
	
	pop	{r2-r5, pc}
	
/*
*setGPIO sets the GPIO, and takes the pin number in r0
*And the value to set the GPIO in r1
*
*/
.global setGPIO
setGPIO:
	push	{r2-r5, lr}
	
	baseAddr	.req	r0	//base address for GPIO
	value		.req	r1	//value to set the pin to
	pin		.req	r2	//pin number
	offSet		.req	r3	//the offset for what set register to use
	data		.req	r4	//data loaded from memory
	
	mov	pin,	r0

	bl	getGPIOAddress
	
	cmp	pin,	#31			//Check if pin is pin 31
	subgt	pin,	#32
	movgt	offSet,	#4
	movle	offSet,	#0

	add	baseAddr,	offSet	//Add base address and offset
//r5 is used as a r4orary storage while the data is changed
	cmp	value,	#1			//Check if the value is 0
	moveq	offSet,	#0x1c		// if value == 0, add offset of clear register
	movne	offSet,	#0x28
	add	baseAddr,	offSet
	ldr	data,	[baseAddr]	//Load address of base address
	mov	r5,	#1
	lsl	r5,	pin		//Left shift r5 by pin
	orr	data,	r5
	str	data,	[baseAddr]
	.unreq	baseAddr
	.unreq	value
	.unreq	pin
	.unreq	offSet
	.unreq	data
	
	pop	{r2-r5, pc}
	
/*
*getGPIO gets the GPIO, and takes the pin number in r0
*It returns the value of the pin in r0
*
*/
.global getGPIO
getGPIO:
	push	{r1-r4, lr}
	
	baseAddr	.req	r0	//base address for GPIO
	pin		.req	r1		//pin number
	offSet		.req	r2	//the offset for what set register to use
	data		.req	r3	//data loaded from memory
	mov	pin,	r0
	bl	getGPIOAddress		//Get the address of the GPIO
//r4 is used as a temporary storage while the data is changed
	cmp	pin,	#31			//Check if pin is pin 31
	subgt	pin,	#32
	movgt	offSet,	#4
	movle	offSet,	#0
	add	baseAddr,	offSet	//Add the offset to the base address
	add	baseAddr,	#0x34	//Add 0x34 to the base address as well.
	ldr	data,	[baseAddr]
	.unreq	baseAddr
	value	.req	r0	//value of the pin requested
	mov	r4,	#1			//Increment temporary storage
	lsl	r4,	pin			//Left shift by the pin
	and	data,	r4		
	cmp	data,	#0
	moveq	value,	#0
	movne	value,	#1
	.unreq	value
	.unreq	pin
	.unreq	offSet
	.unreq	data
	pop	{r1-r4, pc}
/*
*loopWait waits a certain number of micro seconds.
*The number of micro seconds to wait is passed in to r0
*
*/
.globl loopWait
loopWait:
	push	{r1, r2, lr}

	ldr	r1, =0x20003004		// address of timer counter (low 32 bits)
	ldr	r2, [r1]		// r2 = value of timer counter
	add	r2, r0			// r2 = value of timer counter + wait time
theLoop:
	ldr	r0, [r1]		// load current value of timer counter
	cmp	r2, r0			// compare it to the target time
	bge	theLoop			

	pop	{r1, r2, pc}	


.section .data
	
