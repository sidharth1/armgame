.section	.text
/*
*InitMenu provides the initial main menu screen that is displayed at startup of the game.
*It does this by drawing a smaller square within the square 760 times.
*
*/
.globl	InitMenu
InitMenu:
	push	{r4-r7, lr}
	ldr	r4,	=white	//Load color of background
	ldr	r4,	[r4]
	mov	r5,	#0
	mov	r6,	#0
	mov	r7,	#760	//dimension of square
MenuLoop:
	cmp	r7,	#0		//Check if counter has been reached
	ble	DrawOptions	
	mov	r0,	r5		//Set x coordinate
	mov	r1,	r6		//Set y coordinate
	mov	r2,	r4		//Load color into r2
	mov	r3,	r7		//Load dimension of square
	bl	DrawSquare	//Draws a white square
	add	r5,	#1		//Increment x coordinate
	add	r6,	#1		//Increment y coordinate
	sub	r7,	#1		//Decrement counter
	b	MenuLoop
DrawOptions:
	ldr	r0,	=Title	//Load string title of game
	mov	r1,	#300	//X Coordinate
	mov	r2,	#80		//Y Coordinate
	bl	DrawString	//Draws the string
	ldr	r0,	=By		//Load string "By" 
	ldr	r1,	=350	//X Coordinate
	mov	r2,	#100	//Y Coordinate
	bl	DrawString	//Draws the string
	ldr	r0,	=Author1	//Load string containing first Authors name
	mov	r1,	#300	//X Coordinate
	mov	r2,	#120	//Y Coordinate
	bl	DrawString	//Draws the string
	ldr	r0,	=And		//Load string "and"
	ldr	r1,	=355	//X Coordinate
	mov	r2,	#140	//Y Coordinate
	bl	DrawString	//Draws the string
	ldr	r0,	=Author2	//Load string containing second Authors name
	mov	r1,	#300	//X Coordinate
	mov	r2,	#160	//Y Coordinate
	bl	DrawString	//Draws the string
	ldr	r0,	=StartString	//Load string "Start"
	mov	r1,	#300	//X Coordinate
	mov	r2,	#260	//Y Coordinate
	bl	DrawString	//Draws the string
	ldr	r0,	=QuitString		//Load string "Quit"
	mov	r1,	#300	//X Coordinate
	mov	r2,	#360	//Y Coordinate
	bl	DrawString	//Draws the string
endInitMenu$:
	pop	{r4-r7, pc}	//Continues with program
/*
*ClearScreen clears the screen and displays a black screen.
*It does this by drawing a smaller square within the square 760 times.
*/
.globl	ClearScreen
ClearScreen:
	push	{r4-r7, lr}
	mov	r4,	#0		//Load background color black
	mov	r5,	#0		//Staring x coordinate (0)
	mov	r6,	#0		//Staring y coordinate (0)
	mov	r7,	#760	//dimension of square
BlackLoop:
	cmp	r7,	#0		//Check if counter has been reached
	ble	endFillBlack$	//If it has, then screen has been cleared.
	mov	r0,	r5		//Set x coordinate
	mov	r1,	r6		//Set y coordinate
	mov	r2,	r4		//Load color of square
	mov	r3,	r7		//Load dimensions of square
	bl	DrawSquare	//Draws the square
	add	r5,	#1		//Increment x coordinate
	add	r6,	#1		//Increment y coordinate
	sub	r7,	#1		//Decrement counter
	b	BlackLoop
endFillBlack$:
	pop	{r4-r7, pc}	//Continue with program
	
/*
*DrawPointerStart draws a square around the start option in the main menu screen or
*a square around the restart option in the pause menu screen. This indicates to the player
*if this is the selection they want to make.
*/
.globl	DrawPointerStart
DrawPointerStart:
	push	{r4, r5, lr}
	ldr	r4,	=QuitFlag	//Load address of QuitFlag
	mov	r5,	#0
	str	r5,	[r4]		//Set QuitFlag to 0 since we are not on that option
	ldr	r4,	=StartFlag	//Load address of StartFlag
	mov	r5,	#1
	str	r5,	[r4]		//Set StartFlag to 1 since we are on that option
	mov	r0,	#300		//Load x coordinate of square
	ldr	r1,	=385		//Load y coordinate of square
	mov	r2,	#0xFFFFFF	//Load color white
	mov	r3,	#5			//Load dimension of square
	bl	DrawSquare		//Draws a white square next to the "quit" option so that it cannot be seen on the white background
	mov	r0,	#300		//Load x coordinate of square
	ldr	r1,	=285		//Load y coordinate of square
	mov	r2,	#0			//Load color black
	mov	r3,	#5			//Load dimension of square
	bl	DrawSquare		//Draws a black square next to the "start" option so that it indicates to the player that it is the option they are choosing
	pop	{r4, r5, pc}

.globl	DrawPointerQuit
DrawPointerQuit:
	push	{r4, r5, lr}
	ldr	r4,	=QuitFlag	//Load address of QuitFlag
	mov	r5,	#1
	str	r5,	[r4]		//Set QuitFlag to 0 since we are not on that option
	ldr	r4,	=StartFlag	//Load address of StartFlag
	mov	r5,	#0
	str	r5,	[r4]		//Set StartFlag to 1 since we are on that option
	mov	r0,	#300		//Load x coordinate of square
	ldr	r1,	=285		//Load y coordinate of square
	mov	r2,	#0xFFFFFF	//Load color white
	mov	r3,	#5			//Load dimension of square
	bl	DrawSquare		//Draws a white square next to the "start" option so that it cannot be seen on the white background
	mov	r0,	#300		//Load x coordinate of square
	ldr	r1,	=385		//Load y coordinate of square
	mov	r2,	#0			//Load color black
	mov	r3,	#5			//Load dimension of square
	bl	DrawSquare		//Draws a black square next to the "quit" option so that it indicates to the player that it is the option they are choosing
	pop	{r4, r5, pc}

/*
*PauseMenu provides the pause menu screen that is displayed when the player pauses the game.
*It does this by drawing a smaller square within the square 760 times then displaying the options.
*
*/
.globl	PauseMenu
PauseMenu:
	push	{r4-r7, lr}
	ldr	r4,	=white	//Load color of background
	ldr	r4,	[r4]
	mov	r5,	#0
	mov	r6,	#0
	mov	r7,	#760	//dimension of square
PauseMenuLoop:
	cmp	r7,	#0		//Check if counter has been reached
	ble	DrawOptions2
	mov	r0,	r5		//Set x coordinate
	mov	r1,	r6		//Set y coordinate
	mov	r2,	r4		//Load color into r2
	mov	r3,	r7		//Load dimension of square
	bl	DrawSquare	//Draws a white square
	add	r5,	#1		//Increment x coordinate
	add	r6,	#1		//Increment y coordinate
	sub	r7,	#1		//Decrement counter
	b	PauseMenuLoop
DrawOptions2:
	ldr	r0,	=Title	//Load string title of game
	mov	r1,	#300	//X Coordinate
	mov	r2,	#80		//Y Coordinate
	bl	DrawString	//Draws the string
	ldr	r0,	=RestartString	//Load string "Restart"
	mov	r1,	#300	//X Coordinate
	mov	r2,	#260	//Y Coordinate
	bl	DrawString	//Draws the string
	ldr	r0,	=QuitString		//Load string "Quit"
	mov	r1,	#300	//X Coordinate
	mov	r2,	#360	//Y Coordinate
	bl	DrawString	//Draws the string
endPauseMenu$:
	pop	{r4-r7, pc}

.section	.data

.globl	StartString, QuitString, Author1, Author2, And, StartFlag, QuitFlag, RestartString
StartString:	.asciz "Start Game"
QuitString:	.asciz "Quit Game"
Author1:	.asciz "Sidharth Jha"
By:		.asciz "By"
And:		.asciz "&"
Author2:	.asciz "Shamil Ukani"
RestartString:	.asciz "Restart"
QuitFlag:	.int	0	//1 means clear screen and quit game
StartFlag:	.int	0	//1 means start/restart game
