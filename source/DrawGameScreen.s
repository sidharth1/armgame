.section	.text

/*
*InitGameScreen 
*Draws the game screen
*r0 is passed in as current player offset
*/
.globl	InitGameScreen
InitGameScreen:
	push	{r4-r10, lr}
	mov	r8,	r0	//current player offset in r8
	ldr	r9,	=totalKeys	//Load totalKeys so we know how many keys to draw
	ldr	r9,	[r9]
	mov	r0,	#0	//Reset x to 0
	mov	r1,	#0	//Reset y to 0
	mov	r2,	#0xFFFFFF	//Choose colour to be white
	mov	r3,	#40			//Dimension of square is 40 pixels
	bl	DrawGameGrid	
	//Now fill relative to the structure, boardStruct
	ldr	r0,	=boardStruct	//load address of structure
	counter	.req	r4		//counter to traverse a row
	offset	.req	r5		//offset for the boardStruct
	xPos	.req	r6		//x position
	yPos	.req	r7		//y position
	mov	counter,	#0	//counter initialized to 0
	mov	offset,		#0	//offset initialized 0
	mov	xPos,		#0	//x initialized to 0
	mov	yPos,		#0	//x initialized to 0
iterateRow:
	cmp	counter,	#16
	bge	updateRowOffset		//to update the offset to the next row
	cmp	offset,	#256		//16x16=256	
	bge	endInitGameScreen$
	ldr	r0,	=boardStruct
	ldrb	r1,	[r0, offset]
	//check whether tile,wall,key,door,exitdoor, or player to be drawn
	cmp	r1,	#0		//tile
	ldreq	r2,	=white
	cmp	r1,	#1		//wall
	ldreq	r2,	=blue	
	cmp	r1,	#2		//key
	ldreq	r2,	=yellow
	cmp	r1,	#3		//door
	ldreq	r2,	=maroon
	cmp	r1,	#4		//exit door
	ldreq	r2,	=olive
	cmp	r1,	#5		//person
	ldreq	r2,	=green
	//draw the square
	ldr	r2,	[r2]		//load the colour code which passed the test
	bl	xyArgs
	mov	r3,	#1		//number of squares to fill is 1
	bl	FillRow
	//draw characters by comparing current offset to determine what a block is, then proceed to draw
	cmp	r9,	#3	//If keys to be drawn is 3, then go to DrawK3
	beq	DrawK3
	cmp	r9,	#2	//If keys to be drawn is 2, then go to DrawkK2
	beq	DrawK2
	cmp	r9,	#1	//If keys to be drawn is 1, then go to DrawK1
	beq	DrawK1
	b	TEMP
DrawK3:
	cmp	offset, #129	//If the offset is at a key location, then draw key
	beq	DrawK
DrawK2:
	cmp	offset, #51	//If the offset is at a key location, then draw key
	beq	DrawK
DrawK1:
	cmp	offset, #233	//If the offset is at a key location, then draw key
	beq	DrawK
TEMP:
	ldr	r10,	=boardStruct	//Load the board structure
	ldrb	r10,	[r10, offset]
	cmp	offset, #140	//If the offset is at a door location, then draw door
	beq	DrawD
	cmp	offset, #119	//If the offset is at a door location, then draw door
	beq	DrawD
	cmp	offset, #72	//If the offset is at a door location, then draw door
	beq	DrawD
	cmp	offset, #24	//If the offset is at a door location, then draw door
	beq	DrawD
temp2:
	cmp	offset, #254	//If the offset is at an exit door location, then draw exit door
	beq	DrawED
temp3:
	cmp	offset, r8	//If the offset is at the player location, then draw person
	beq	DrawP
itr:	//update and branch back to iterate to the next row
	add	xPos,	#40		//increment xPos to new element	
	ldr	r0,	=boardStruct
	add	offset,  #1		//to next value in the row
	add	counter, #1
	b	iterateRow
updateRowOffset:
	mov	xPos,		#0
	add	yPos,		#40
	mov	counter,	#0
	ldr	r0,	=boardStruct
	b	iterateRow
DrawD:
	cmp	r10,	#3		//Draw a door
	bne	temp2
	mov	r2,	#'D'		//move character D into r2
	bl	xyArgs
	bl	DrawChar		//Draw character D
	b	itr

DrawK:
	mov	r2,	#'K'		//Move character K into r2
	bl	xyArgs
	bl	DrawChar		//Draw character K
	b	itr


DrawP:
	mov	r2,	#'P'		//Move character P into r2
	bl	xyArgs
	bl	DrawChar		//Draw character P
	b	itr
DrawED:
	cmp	r10,	#4	
	bne	temp3
	mov	r2,	#'E'		//Move E into r2
	bl	xyArgs
	bl	DrawChar		//Draw character E
	b	itr
xyArgs:
	mov	r0,	xPos
	mov	r1,	yPos
	mov	pc,	lr
endInitGameScreen$:
	.unreq	counter
	.unreq	xPos
	.unreq	yPos
	.unreq	offset
	ldr	r0,	=Title		//Move string into r0
	ldr	r1,	=240		//x location of string
	ldr	r2,	=640		//y location of string
	bl	DrawString		//Draw the string
	ldr	r0,	=Moves		//Move number of moves into r0
	ldr	r1,	=240		//x location of string
	ldr	r2,	=660		//y location of string
	bl	DrawString		//Draw the string
//
	ldr	r0,	=Keys		//Move string into r0
	ldr	r1,	=240		//x location of string
	ldr	r2,	=680		//y location of string
	bl	DrawString		//Draw the string

	pop	{r4-r10, pc}

.section	.data
.globl	NewBoardFlag, Keys
NewBoardFlag:	.int	0	//1 means draw new board
Keys:		.asciz	"Keys Picked: Z"


