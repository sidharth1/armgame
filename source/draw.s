.section	.text

/*
*FillRow fills a row of squares from a starting point (x,y) for a certain number of squares to fill of a certain color
*Takes input r0 is x value for starting point, r1 is the y value 
*r2 is the color to be filled and r3 is the number of squares to fill
*/

.globl	FillRow
FillRow:
	push	{r4-r8, lr}
	add	r0,	#2	//Add 2 to the starting x so that way the outline of the boxes can be seen
	add	r1,	#2	//Add 2 to the starting y so that way the outline of the boxes can be seen
	mov	r4,	r0	//x start
	mov	r5,	r1	//y start
	mov	r6,	r2	//colour to fill
	mov	r8,	r3	//number of squares to fill
	mov	r3,	#36	//Fill Dimension
	mov	r7,	#0	//counter
FillRowLoop:
	cmp	r7,	r8	//Check if the target number of squares has been reached
	bge	endFillRow	//If it has, continue with program
	bl	FillSquare	//Fill the square of current x and y
	mov	r1,	r5	//reset y
	add	r4,	#40	//move +40 on x axis to move to next horizontal square
	mov	r0,	r4
	mov	r2,	r6	//reset colour
	mov	r3,	#36	//reset fill dimension
	add	r7,	#1	//Increment counter
	b	FillRowLoop
endFillRow:
	pop	{r4-r8, pc}

/*
*FillCol fills a column of squares from a starting point (x,y) for a certain number of squares to fill of a certain color
*Takes input r0 is x value for starting point, r1 is the y value 
*r2 is the color to be filled and r3 is the number of squares to fill
*/
.globl	FillCol
FillCol:
	push	{r4-r8, lr}
	add	r0,	#2	//Add 2 to the starting x so that way the outline of the boxes can be seen
	add	r1,	#2	//Add 2 to the starting y so that way the outline of the boxes can be seen
	mov	r4,	r0	//x start
	mov	r5,	r1	//y start
	mov	r6,	r2	//colour to fill
	mov	r8,	r3	//number of squares to fill
	mov	r3,	#36	//Fill Dimension
	mov	r7,	#0	//counter
FillColLoop:
	cmp	r7,	r8	//Check if the target number of squares has been reached
	bge	endFillCol	//If it has, continue with program
	bl	FillSquare	//Fill the square of current x and y
	mov	r0,	r4	//reset x
	add	r5,	#40	//move +40 on y axis to move to the next vertical square
	mov	r1,	r5
	mov	r2,	r6	//reset colour
	mov	r3,	#36	//reset fill dimension
	add	r7,	#1	//increment counter
	b	FillColLoop
endFillCol:
	pop	{r4-r8, pc}
/*
*DrawGameGrid draws the outline of the grid
*r2 is the color to be filled and r3 is the number of squares to fill
*/


.globl	DrawGameGrid
DrawGameGrid:
	push	{r4-r10, lr}
	mov	r10,	r2	//colour
	mov	r9,	#0	//line counter (goes up till 15)
	mov	r4,	#256	//counter
	mov	r5,	#0	//to reset x
	mov	r6,	#0	//to reset y
	mov	r7,	r3	//dimension of square
DrawGrid:
	cmp	r4,	#0	//Check if counter has been reached
	bleq	endDrawGrid	//If it has, continue with program
	cmp	r9,	#16	//Check if line counter has been reached
	bge	resetXY	//If it is larger than or equal to line counter, resetXY
	mov	r0,	r5	//Move 0 into x
	mov	r1,	r6	//Move 0 into y
	mov	r2,	r10	//Move color back into r2
	mov	r3,	r7	//Move dimension of square back into r3
	bl	DrawSquare	//Draw a square
	add	r5,	r7	//Add dimension of square to x value
	sub	r4,	#1	//Decrement counter
	add	r9,	#1	//Increment line counter
	b	DrawGrid
resetXY:
	mov	r5,	#0	//Reset x
	add	r6,	r7	//Add dimension of square to y value
	mov	r9,	#0	//Reset line counter
	b	DrawGrid
endDrawGrid:
	pop	{r4-r10, pc}

/*
*FillSquare fills the square with a certain color. It does this by drawing a square, then decreasing the dimensions of the square by one.
*It essentially draws a smaller square within a square 38 times.
*Takes input r0 is x value for starting point, r1 is the y value 
*r2 is the color to be filled and r3 is the dimension of the square
*/
.globl	FillSquare
FillSquare:
	push	{r4-r8, lr}
	mov	r4,	r0	//x start
	mov	r5,	r1	//y start
	mov	r6,	r2	//colour value
	mov	r7,	r3	//dimension of fill
	mov	r8,	#0	//counter
FillLoop:
	cmp	r8,	#38	//draw square 38 times
	bge	endFillLoop	//When counter is reached, continue with program
	bl	DrawSquare	//Draw square
	sub	r7,	#1
	mov	r3,	r7	//dimension - 1
	mov	r2,	r6	//colour
	add	r5,	#1
	mov	r1,	r5	// y start pos + 1
	add	r4,	#1	
	mov	r0,	r4	//x start pos + 1
	add	r8,	#1	//increment counter
	b	FillLoop
endFillLoop:
	pop	{r4-r8, pc}

/*
*Draws a horizontal line at the at the starting point for moving right for the number of pixels indicated.
*Takes input r0 is x value for starting point, r1 is the y value 
*r2 is the color to be filled and r3 is the length of the line
*/

.globl	HorLine
HorLine:
	push	{r4-r9, lr}
	px	.req	r0	//x value
	py	.req	r1	//y value
	x	.req	r4
	y	.req	r5
	colour	.req	r7
	mov	r9,	#0		//counter number of pixels drawn
	mov	x,	px	
	mov	y,	py
	mov	colour, r2	//Pass in color
	mov	r6,	#0
	add	r6,	r3		//r6 stores number of pixels to be drawn
Loopy2:
	cmp	r9,	r6		//Check if number of pixels to be drawn has been reached
	bge	done2		//If it has, go to done2
	mov	px,	x		//Store x in px
	mov	py,	y		//Store y in py
	.unreq	px
	.unreq	py
	.unreq	x
	.unreq	y
	.unreq	colour
	mov	r2,	r7		//Move color into r2
	bl	DrawPixel	//Draw a pixel
	px	.req	r0	//x value
	py	.req	r1	//y value
	x	.req	r4
	y	.req	r5
	colour	.req	r7
	add 	r9, #1		//increment counter for number of pixels drawn
	add	x, #1		//Increment x value
	b	Loopy2
done2:
	.unreq	px
	.unreq	py
	.unreq	colour
	pop	{r4-r9, pc}		//Continue with program

/*
*Draws a vertical line at the at the starting point for moving down for the number of pixels indicated.
*Takes input r0 is x value for starting point, r1 is the y value 
*r2 is the color to be filled and r3 is the length of the line
*/	
	
.globl	VertLine
VertLine:
	push	{r4-r9, lr}
	px	.req	r0	//x value
	py	.req	r1	//y value
	x	.req	r4
	y	.req	r5
	colour	.req	r7
	mov	r9,	#0	//counter for number of pixels drawn
	mov	x,	px
	mov	y,	py
	mov	colour, r2	//pass in colour
	mov	r6,	#0
	add	r6,	r3	//r6 stores number of pixels to be drawn
Loopy:
	cmp	r9,	r6	//Check if number of pixels to be drawn has been reached
	bge	done	//If so, go to done
	mov	px,	x	//store x in px
	mov	py,	y	//Store y in py
	.unreq	px
	.unreq	py
	.unreq	x
	.unreq	y
	.unreq	colour
	mov	r2,	r7	//Move colour into r2
	bl	DrawPixel	//Draw pixel
	px	.req	r0	//x value
	py	.req	r1	//y value
	x	.req	r4
	y	.req	r5
	colour	.req	r7
	add 	r9, #1		//increment counter for number of pixels drawn
	add	y, #1
	b	Loopy
done:
	.unreq	px
	.unreq	py
	.unreq	colour
	pop	{r4-r9, pc}	//Continue with program
/*
*Draws a square from starting coordinates (x,y) for the specified colour and size
*Takes input r0 is x value for starting point, r1 is the y value 
*r2 is the color to be filled and r3 is the dimension of the square
*/
.globl	DrawSquare
DrawSquare:
	push {r4-r9, lr}
	counter	.req	r4
	mov	r5,	r0	//px starts at r0
	mov	r7,	r0	//save value of r0 in r7
	mov	r6,	r1	//save value of r1 in r6
	mov	r8,	r2	//Move colour into r8
	mov	r9,	r3	//r3 is dimension of the square
	mov	counter, #0	//Reset counter
VGrid:
	cmp	counter, #2	//Check if counter has been reached
	bge	doneVGrid$	//If so, then go to draw horizontal line
	mov	r2,	r8		//Move colour back into r2
	mov	r3,	r9		//Move dimension of square back into r3
	bl	VertLine	//Draw a line for the color and dimension specified
	add	r5,	r9		//Add the dimension of the square to current x to get the new x value so we can draw a second horizontal line
	mov	r0,	r5		//move x value to be new x
	add	counter, #1	//increment counter
	mov	r1,	r6		//Move y value back into r1
	b	VGrid

doneVGrid$:
	mov	r1,	r6	//Set original y
	mov	r5,	r1	//Save value of py
	mov	r0,	r7	//Set original x
	mov	counter, #0	//Reset counter
HGrid:
	cmp	counter, #2	//Check if counter has been reached
	bge	doneHGrid$	//If so, then continue with program
	mov	r2,	r8		//Move colour back into r2
	mov	r3,	r9		//Move dimension of square back into r3
	bl	HorLine		//Draw a line for the color and dimension specified
	add	r5,	r9		//Add the dimension of the square to current y to get the new y value so we can draw a second vertical line
	mov	r1,	r5		//Move y to be the new y
	add	counter, #1	//increment counter
	mov	r0,	r7		//move x back into r0
	b	HGrid
doneHGrid$:
	pop	{r4-r9, pc}	//Continue with program


/* Draw the character 'B' to (0,0)
* r0 and r1 as x and y start and r2 as character e.g. #'E'
 */
.globl	DrawChar
DrawChar:
	push	{r4-r10, lr}

	chAdr	.req	r4
	px		.req	r5
	py		.req	r6
	row		.req	r7
	mask	.req	r8
	mov	r9,	r0
	add	r9,	#20
	mov	r10,	r1
	add	r10,	#20
	mov	py,	r10
	mov	r10,	r2
	
	ldr		chAdr,	=font		// load the address of the font map
	mov		r0,		r10		// load the character into r0
	add		chAdr,	r0, lsl #4	// char address = font base + (char * 16)
				// init the Y coordinate (pixel coordinate)


charLoop$:
	mov		px,		r9			// init the X coordinate

	mov		mask,	#0x01		// set the bitmask to 1 in the LSB
	
	ldrb	row,	[chAdr], #1	// load the row byte, post increment chAdr

rowLoop$:
	tst		row,	mask		// test row byte against the bitmask
	beq		noPixel$

	mov		r0,		px
	mov		r1,		py
	mov		r2,		#0xf800	// red
	bl		DrawPixel			// draw red pixel at (px, py)

noPixel$:
	add		px,		#1			// increment x coordinate by 1
	lsl		mask,	#1			// shift bitmask left by 1

	tst		mask,	#0x100		// test if the bitmask has shifted 8 times (test 9th bit)
	beq		rowLoop$

	add		py,		#1			// increment y coordinate by 1

	tst		chAdr,	#0xF
	bne		charLoop$			// loop back to charLoop$, unless address evenly divisible by 16 (ie: at the next char)

	.unreq	chAdr
	.unreq	px
	.unreq	py
	.unreq	row
	.unreq	mask

	pop		{r4-r10, pc}

.section .data

.align 4
font:		.incbin	"font.bin"
